import os
import tkinter as tk
import threading
from tkinter import ttk
from tkinter.filedialog import askopenfilename
from CTReconstruction import ReconstructionMethod, CTReconstruction
from CTPlotting import CTPlot


class App:

    global ct_reconstruct
    ct_reconstruct = CTReconstruction()

    global ct_plot
    ct_plot = CTPlot(ct_reconstruct)

    def __init__(self, root):
        #setting title
        root.title("CT Reconstruction simulation")
        #setting window size
        width=800
        height=500
        screenwidth = root.winfo_screenwidth()
        screenheight = root.winfo_screenheight()
        alignstr = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        root.geometry(alignstr)
        root.resizable(width=False, height=False)
        

        # Create value lists
        self.reconstruction_methods = [ReconstructionMethod.back_projection.value,
                                       ReconstructionMethod.back_projection_fft.value,
                                       ReconstructionMethod.back_projection_Hamming.value,
                                       ReconstructionMethod.skimage_filtered.value,
                                       ReconstructionMethod.skimage_Hamming.value,
                                       ReconstructionMethod.compare_FBP.value,
                                       ReconstructionMethod.compare_Hamming.value]
        self.opt_method_var = tk.StringVar(value=self.reconstruction_methods[0])
        

        # Create control variables
        self.plot_projections_var = tk.BooleanVar()
        self.plot_sinogram_var = tk.BooleanVar()
        
        
        self.projection_range = ["90°", "180°", "360°"]
        self.projection_angle = ["1°","2°","3°","4°","5°","6°","7°","8°","9°","10°","15°","20°","25°","30°","45°"]
        self.opt_range_var = tk.StringVar(value=self.projection_range[1])
        self.opt_angle_var = tk.StringVar(value=self.projection_angle[4])

        self.progress_var = tk.DoubleVar(value=0.0)

        # Set the initial theme
        root.tk.call("source", "azure.tcl")
        root.tk.call("set_theme", "light")

        self.setup_widgets()

    def setup_widgets(self):
        
        basic_frame = ttk.LabelFrame(root, text="Basic information", padding=(20, 10))
        basic_frame.grid(row=0, column=0, padx=(20, 10), pady=(20, 10), sticky="nsw")

        label_input = ttk.Label(basic_frame, text='Input image:', justify='left')
        label_input.grid(row=0, column=0, padx=5, pady=10, sticky="w")  # Adjusted 'sticky' to 'w' (west)

        self.txt_input = ttk.Entry(basic_frame, width=45)
        self.txt_input.grid(row=0, column=1, columnspan=4, padx=5, pady=10, sticky="ew")  # 'columnspan' increased to 2, 'sticky' adjusted to 'ew'

        label_methods = ttk.Label(basic_frame, text='Reconstruction methods:', justify='left')
        label_methods.grid(row=1, column=0, padx=5, pady=10, sticky="w")  # Adjusted 'sticky' to 'w' (west)

        btn_select = ttk.Button(basic_frame, text='Select', style='Accent.TButton', command=self.load_image)
        btn_select.grid(row=0, column=5, padx=5, pady=10, )  # 'sticky' adjusted to 'ew'

        btn_show = ttk.Button(basic_frame, text='Show', command=ct_plot.show_image)
        btn_show.grid(row=0, column=6, padx=5, pady=10, sticky="ew")  # 'sticky' adjusted to 'ew'

        opt_methods = ttk.OptionMenu(basic_frame, self.opt_method_var, ReconstructionMethod.back_projection.value, *self.reconstruction_methods)
        opt_methods.grid(row=1, column=1, columnspan=3, padx=5, pady=10, sticky="ew")  # 'columnspan' increased to 3, 'sticky' adjusted to 'ew'
        # Set a trace on the variable to call a function when its value changes and pass the selected value
        self.opt_method_var.trace("w", lambda *args: ct_reconstruct.set_reconstruction_method(self.opt_method_var.get()))
        
        
        fine_tuning_frame = ttk.LabelFrame(root, text="Fine tuning", padding=(20, 10), width=120)
        fine_tuning_frame.grid(row=1, column=0, padx=(20, 10), pady=10, sticky='nsw')

        label_plot_projects = ttk.Label(fine_tuning_frame, text='Plot projections:', justify='left')
        label_plot_projects.grid(row=0, column=0, padx=5, pady=10)

        checkbox_projects = ttk.Checkbutton(fine_tuning_frame, variable=self.plot_projections_var, text='(not for skimage methods)',
                                            command=lambda *args: ct_reconstruct.set_plot_projections(self.plot_projections_var.get()))
        checkbox_projects.grid(row=0, column=1, padx=5, pady=10)

        label_plot_sinogram = ttk.Label(fine_tuning_frame, text='Plot sinogram(s):', justify='left')
        label_plot_sinogram.grid(row=1, column=0, padx=5, pady=10, sticky="ew")
        
        checkbox_sinogram = ttk.Checkbutton(fine_tuning_frame, variable=self.plot_sinogram_var,
                                            command=lambda *args: ct_reconstruct.set_plot_sinogram(self.plot_sinogram_var.get()))
        checkbox_sinogram.grid(row=1, column=1, padx=5, pady=10, sticky="ew")

        label_projects_range = ttk.Label(fine_tuning_frame, text='Projection range:', justify='left')
        label_projects_range.grid(row=0, column=2, padx=(120,5), pady=10, sticky='w')  # Set sticky to 'ew' for stretching

        opt_projects_range = ttk.OptionMenu(fine_tuning_frame, self.opt_range_var, self.projection_range[1], *self.projection_range)
        opt_projects_range.grid(row=0, column=3, padx=(10,97), pady=10, sticky='e')  # Set sticky to 'ew' to match label width
        self.opt_range_var.trace("w", lambda *args: ct_reconstruct.set_projection_range(int(self.opt_range_var.get().replace("°", ""))))


        label_projects_angle = ttk.Label(fine_tuning_frame, text='Projection angle:', justify='left')
        label_projects_angle.grid(row=1, column=2, padx=(120,5), pady=10)  # Set sticky to 'ew' for stretching
        
        opt_projects_angle = ttk.OptionMenu(fine_tuning_frame, self.opt_angle_var, self.projection_angle[4], *self.projection_angle)
        opt_projects_angle.grid(row=1, column=3, padx=(10,97), pady=10, sticky="ew")  # Adjust 'sticky' to 'ew
        self.opt_angle_var.trace("w", lambda *args: ct_reconstruct.set_projection_angle(int(self.opt_angle_var.get().replace("°", ""))))
 
        # Separator
        separator = ttk.Separator(root)
        separator.grid(row=2, column=0, padx=(20, 45), pady=10, sticky="ew")

        reconstruct_frame = ttk.Frame(root, border=0, padding=(20, 10))
        reconstruct_frame.grid(row=4, column=0, padx=(20, 10), pady=10, sticky="nsew")

        label_status = ttk.Label(reconstruct_frame, text='Status:', justify='left')
        label_status.grid(row=0, column=0, padx=5, pady=5, sticky='nsew')

        self.label_status_info = ttk.Label(reconstruct_frame, text='on stand by', justify='left')
        self.label_status_info.grid(row=0, column=1, padx=5, pady=5, sticky='ew')
        
        # Progressbar
        self.progress = ttk.Progressbar(
            reconstruct_frame, value=0, variable=self.progress_var, mode="indeterminate", length=550
        )
        self.progress.grid(row=1, column=0, columnspan=20, pady=20)


        btn_reconstruct = ttk.Button(reconstruct_frame, text='Reconstruct', style='Accent.TButton', command=self.select_reconstruction)
        btn_reconstruct.grid(row=1, column=21, padx=50)


    #Function fills texfield with name of an image
    def fill_image_textfield(self, image_path):
        self.txt_input.delete(0, tk.END)
        self.txt_input.insert(tk.INSERT, image_path)

    #Function loads image to program, allowed extension are PNG, BMP
    def load_image(self):
        image_path = askopenfilename(filetypes=(("PNG", "*.png"),("BMP", "*.bmp")))
        filename = os.path.basename(image_path)
        ct_reconstruct.load_image(image_path, filename)
        self.fill_image_textfield(filename)

    def select_reconstruction(self):
        if ct_reconstruct._reconstruct_method is ReconstructionMethod.back_projection:
            self.start_BP_async()
        elif(ct_reconstruct._reconstruct_method is ReconstructionMethod.back_projection_fft):
            self.start_fBP_async()
        elif(ct_reconstruct._reconstruct_method is ReconstructionMethod.back_projection_Hamming):
            self.start_fBP_hamming_async()
        elif(ct_reconstruct._reconstruct_method is ReconstructionMethod.skimage_filtered):
            self.start_skimage_lib_async(False)
        elif(ct_reconstruct._reconstruct_method is ReconstructionMethod.skimage_Hamming):
            self.start_skimage_lib_async(True)
        elif(ct_reconstruct._reconstruct_method is ReconstructionMethod.compare_FBP):
            self.start_fBP_comparison_async()
        elif(ct_reconstruct._reconstruct_method is  ReconstructionMethod.compare_Hamming):
            ct_reconstruct.calculate_comparison_hamming()


    def start_BP_async(self):
        self.label_status_info.config(text="Reconstructing input image using back projection...")
        self.progress.start()
        root.update_idletasks()
        ct_reconstruct.create_reconstruction_space()
        ct_reconstruct.prepare_reconstruction_space()

        
        if(ct_reconstruct._plot_projections == True):
            ct_plot.plot_projections()

        if(ct_reconstruct._plot_sinogram == True):
            ct_plot.plot_sinogram()


        calculation_thread = threading.Thread(target=self.calculate_BP_with_callback)
        calculation_thread.start()

    def start_fBP_async(self):
        self.label_status_info.config(text="Reconstructing input image using filtered BP via fft...")
        self.progress.start()
        root.update_idletasks()
        ct_reconstruct.create_reconstruction_space()
        ct_reconstruct.prepare_reconstruction_space()

        if(ct_reconstruct._plot_projections == True):
            ct_plot.plot_projections()

        if(ct_reconstruct._plot_sinogram == True):
            ct_plot.plot_sinogram()

        calculation_thread = threading.Thread(target=self.calculate_fBP_with_callback)
        calculation_thread.start()

    def start_fBP_hamming_async(self):
        self.label_status_info.config(text="Reconstructing input image using filtered BP via Hamming...")
        self.progress.start()
        root.update_idletasks()
        ct_reconstruct.create_reconstruction_space()
        ct_reconstruct.prepare_reconstruction_space()
        
        if(ct_reconstruct._plot_projections == True):
            ct_plot.plot_projections()

        if(ct_reconstruct._plot_sinogram == True):
            ct_plot.plot_sinogram()


        calculation_thread = threading.Thread(target=self.calculate_fBP_hamming_with_callback)
        calculation_thread.start()
 
    def start_skimage_lib_async(self, hamming):
        self.label_status_info.config(text="Reconstructing input image using skimage library...")
        self.progress.start()
        root.update_idletasks()
        ct_reconstruct.create_reconstruction_space()

        if(ct_reconstruct._plot_sinogram == True):
            ct_plot.plot_skimage_sinogram()

        calculation_thread = threading.Thread(target=lambda *args: self.calculate_fBP_skimage_lib_with_callback(hamming))
        calculation_thread.start()

    def start_fBP_comparison_async(self):
        self.label_status_info.config(text="Reconstructing input image for comparison (this might take a while)...")
        self.progress.start()
        root.update_idletasks()
        ct_reconstruct.create_reconstruction_space()
        ct_reconstruct.prepare_reconstruction_space()

        if(ct_reconstruct._plot_projections == True):
            ct_plot.plot_projections()


        calculation_thread = threading.Thread(target=self.calculate_comparison_fBP_with_callback)
        calculation_thread.start()

    def calculate_BP_with_callback(self):
        ct_reconstruct.calculate_BP()
        #Upon completion, trigger the update in the GUI
        root.after(0, lambda *args: self.update_gui_after_computation(ct_reconstruct.fbp_rotated, 'Back Projection via Summation Method', 1))

    def calculate_fBP_with_callback(self):
        ct_reconstruct.calculate_fBP()
        root.after(0, lambda *args: self.update_gui_after_computation(ct_reconstruct.f_rotated, 'Filtered Back Projection via FST', 2))

    def calculate_fBP_hamming_with_callback(self):
        ct_reconstruct.calculate_fbp_hamming()
        root.after(0, lambda *args: self.update_gui_after_computation(ct_reconstruct.fbp_rotated_hamming, 'Filtered Back Projection via Hamming', 3))

    def calculate_fBP_skimage_lib_with_callback(self, hamming):
        ct_reconstruct.calculate_fbp_skimage_lib(hamming)

        if(hamming == True):
            root.after(0, lambda *args: self.update_gui_after_computation(ct_reconstruct.reconstruction_img_rotated, 'Reconstructed image via Skimage lib (Hamming)', 5))
        else:
            root.after(0, lambda *args: self.update_gui_after_computation(ct_reconstruct.reconstruction_img_rotated, 'Reconstructed image via Skimage lib', 4))


    def calculate_comparison_fBP_with_callback(self):
        ct_reconstruct.calculate_comparison_fbp()
        root.after(0, self.update_gui_after_computation_compare)

        

    def update_gui_after_computation(self, reconstructed_img, title, ID):
        self.progress.stop()  # Stop the progress bar
        self.label_status_info.config(text="Done!")
        ct_plot.plot_reconstructed_image(reconstructed_img, title, ID)
       
    def update_gui_after_computation_compare(self):
        self.progress.stop()  # Stop the progress bar
        self.label_status_info.config(text="Comparison done!")

        if(ct_reconstruct._plot_sinogram == True):
            ct_plot.plot_compare_sinograms()

        ct_plot.plot_compare_reconstructed_images()

if __name__ == "__main__":
    root = tk.Tk()
    app = App(root)
    root.mainloop()
