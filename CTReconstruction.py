# @author Kment 
from PIL import Image
import numpy as np

from enum import Enum
from scipy.interpolate import RectBivariateSpline
from scipy.fft import fft, ifft
from skimage.transform import radon, iradon, rotate

class ReconstructionMethod(Enum):
    back_projection = "Back projection (1)"
    back_projection_fft = "Filtered BP via FST (2)"
    back_projection_Hamming = "Filtered BP via Hamming (3)"
    skimage_filtered = "Filtered BP via skimage library (4)"
    skimage_Hamming = "Filtered BP via skimage's Hamming (5)"
    compare_FBP = "Comparison (2) vs (4)"
    compare_Hamming = "Comparison (1) vs (3)"


class CTReconstruction:

    def __init__(self) -> None:
        self._reconstruct_method = ReconstructionMethod.back_projection
        self._plot_projections = False
        self._plot_sinogram = False
        self._projection_range = 180
        self._projection_angle = 5

    def set_reconstruction_method(self, method):
        print(method)
        self._reconstruct_method = ReconstructionMethod(method)
        print(self._reconstruct_method)

    def set_plot_projections(self, plot_projections):
        print(self._plot_projections)
        self._plot_projections = plot_projections
        print(self._plot_projections)

    def set_plot_sinogram(self, plot_sinogram):
        print(self._plot_sinogram)
        self._plot_sinogram = plot_sinogram
        print(self._plot_sinogram)

    def set_projection_range(self, projection_range):
        print(self._projection_range)
        self._projection_range = projection_range
        print(self._projection_range)

    def set_projection_angle(self, projection_angle):
        print(self._projection_angle)
        self._projection_angle = projection_angle
        print(self._projection_angle)

    #Function load input image for further processing
    def load_image(self, image_path, filename):
        self._img = Image.open(image_path)
        # I can check if it's grayscale and if not then convert it
        self._img = self._img.convert('L')
        self._name = filename


    def create_reconstruction_space(self):
        # Resize Image
        diag = len(np.diag(self._img)//2)
        self.image = np.pad(self._img, pad_width=diag+10)
        self._ = np.linspace(-1, 1, self.image.shape[0])
        self.xv, self.yv = np.meshgrid(self._,self._)

    def prepare_reconstruction_space(self):
        #here lies a problem and it needs to be solved
        self.thetas = np.arange(0, self._projection_range, self._projection_angle) * np.pi / self._projection_range
        self.rs = self._
        self.dtheta = np.diff(self.thetas)[0]
        self.dr = np.diff(self.thetas)[0]
        self.rotations = np.array([rotate(self.image, theta * self._projection_range / np.pi) for theta in self.thetas])
        self.p = np.array([rotation.sum(axis=0) * self.dr for rotation in self.rotations]).T

    def calculate_BP(self):
        p_interp = RectBivariateSpline(self.rs, self.thetas, self.p)
        self.fBP = np.vectorize(self.get_f)(self.xv,self.yv, p_interp)
        # Rotate the fBP image to align it with the original image
        self.fbp_rotated = rotate(self.fBP, -180)


    def calculate_fBP(self):
        P = fft(self.p, axis=0)
        nu = np.fft.fftfreq(P.shape[0], d=np.diff(self.rs)[0])

        integrand = P.T * np.abs(nu)
        integrand = integrand.T
        p_p = np.real(ifft(integrand, axis=0))
        p_p_interp = RectBivariateSpline(self.rs, self.thetas, p_p)
        f = np.vectorize(self.get_f)(self.xv,self.yv, p_p_interp)
        self.f_rotated = rotate(f, -180)
        

    def get_f(self, x,y, p_interp):
        return p_interp(x*np.cos(self.thetas)+y*np.sin(self.thetas), self.thetas, grid=False).sum() * self.dtheta


    def hamming_window(self, N):
        return 0.54 - 0.46 * np.cos(2 * np.pi * np.arange(N) / (N - 1))

    def apply_hamming_filter(self):
        hamming_window = self.hamming_window(len(self.rs))

        # Apply Hamming window to each projection
        for i in range(self.p.shape[1]):  # Assuming self.p has shape (rs, thetas)
            self.p[:, i] *= hamming_window

    def calculate_fbp_hamming(self):

        # Apply the Hamming filter to the projections
        self.apply_hamming_filter()

        p_interp = RectBivariateSpline(self.rs, self.thetas, self.p)
        fBP = np.vectorize(self.get_f)(self.xv, self.yv, p_interp)
        self.fbp_rotated_hamming = rotate(fBP, -180)


    def calculate_fbp_skimage_lib(self, hamming):
        
        theta = np.arange(0., self._projection_range, self._projection_angle)
        self.sinogram = radon(self.image, theta=theta)

        if(hamming == True):
            reconstruction_img = iradon(self.sinogram, theta=theta, filter_name='hamming')
        else:
            reconstruction_img = iradon(self.sinogram, theta=theta, filter_name='ramp')
            
        self.reconstruction_img_rotated =  rotate(reconstruction_img, -180)

            

    def calculate_comparison_fbp(self):
        self.calculate_fBP()
        self.calculate_fbp_skimage_lib(False)


   
    def calculate_comparison_hamming(self):
        self.calculate_BP()
        se

