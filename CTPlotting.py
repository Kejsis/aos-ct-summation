import matplotlib.pyplot as plt
from CTReconstruction import CTReconstruction
import numpy as np

class CTPlot:

    _ct_reconstruction = CTReconstruction()

    def __init__(self, ct_reconstruction):
        self._ct_reconstruction = ct_reconstruction

    def show_image(self):
        plt.figure(111)
        plt.suptitle(self._ct_reconstruction._name)
        plt.imshow(self._ct_reconstruction._img, cmap='gray')
        plt.gcf().canvas.manager.set_window_title('Input Image')
        plt.show()

    def plot_projections(self):

        plt.imshow(self._ct_reconstruction.rotations[1])
        plt.show()

        plt.imshow(self._ct_reconstruction.rotations[8])
        plt.show()
        plt.imshow(self._ct_reconstruction.rotations[12])
        plt.show()
        plt.imshow(self._ct_reconstruction.rotations[16])
        plt.show()
        plt.imshow(self._ct_reconstruction.rotations[20])
        plt.show()
        plt.imshow(self._ct_reconstruction.rotations[30])
        plt.show()
        plt.imshow(self._ct_reconstruction.rotations[35])
        plt.show()
        # Create a figure
        fig, ax = plt.subplots(figsize=(15, 8))

        # Iterate through the array elements and plot them with different colors
        for i, rotation in enumerate(self._ct_reconstruction.rotations):
            p = rotation.sum(axis=0) * self._ct_reconstruction.dr
            ax.plot(self._ct_reconstruction.rs, p, label=f'Projection {i + 1}')

        # Add a legend to differentiate the elements by labels
        ax.legend(loc='upper left', bbox_to_anchor=(1,1), fontsize=8)
        ax.set_xlabel('r', fontsize=14)
        ax.set_ylabel('$\ln(I_0/I)$', fontsize=14)

        # Set a title for the plot
        ax.set_title('Multiple Projections in One Plot')
        plt.show()

    def plot_compare_sinograms(self):

        fig, axes = plt.subplots(1, 2, figsize=(12, 6))

        # Plot for the first subplot
        ax1 = axes[0]
        img1 = ax1.pcolor(self._ct_reconstruction.sinogram, cmap='gray')
        ax1.set_title('Sinogram of input image via Skimage lib')


        # Plot for the second subplot
        ax2 = axes[1]
        img2 = ax2.pcolor(self._ct_reconstruction.thetas, self._ct_reconstruction.rs, self._ct_reconstruction.p, cmap='gray')
        ax2.set_xlabel(r'$\theta$', fontsize=14)
        ax2.set_ylabel('$r$', fontsize=14)
        ax2.set_title('Sinogram of input image via me')

    def plot_compare_reconstructed_images(self):
        fig, axes = plt.subplots(1, 2, figsize=(12, 6))

        # Plot for the first subplot
        ax1 = axes[0]
        img1 = ax1.pcolor(self._ct_reconstruction.reconstruction_img_rotated, cmap='gray')
        ax1.set_title('Reconstructed image via Skimage lib')
        ax1.set_xlim(0, self._ct_reconstruction.reconstruction_img_rotated.shape[1])  # Setting x-axis limit
        ax1.set_ylim(0, self._ct_reconstruction.reconstruction_img_rotated.shape[0])  # Setting y-axis limit

        # Plot for the second subplot
        ax2 = axes[1]
        img2 = ax2.pcolor(self._ct_reconstruction.f_rotated, cmap='gray')
        ax2.set_title('Reconstructed image via me')
        ax2.set_xlim(0, self._ct_reconstruction.f_rotated.shape[1])  # Setting x-axis limit
        ax2.set_ylim(0, self._ct_reconstruction.f_rotated.shape[0])  # Setting y-axis limit

        # Adjust layout to prevent overlapping of values
        plt.tight_layout()
        plt.show()

    def plot_sinogram(self):
        plt.pcolor(self._ct_reconstruction.thetas, self._ct_reconstruction.rs, self._ct_reconstruction.p, shading='auto', cmap='gray')
        plt.xlabel(r'$\theta$', fontsize=14)
        plt.ylabel('$r$', fontsize=14)
        plt.title('Sinogram of input image')
        plt.show()

    def plot_skimage_sinogram(self):
        plt.pcolor(self._ct_reconstruction.sinogram, shading='auto', cmap='gray')
        plt.title('Sinogram of input image via Skimage lib')
        plt.show()
    


    def plot_reconstructed_image(self, image, title, ID):
        plt.figure(ID, figsize=(6,6))
        plt.pcolor(image, cmap='gray')
        plt.title(title)
        plt.show()
